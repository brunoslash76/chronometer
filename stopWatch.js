var play = true;
var paused = true;
var timer = 0;
var mili = 0;
var seg = 0;
var min = 0;
var hora = 0;
var lap = '';
var btnPlayStop = document.getElementById('play-stop');
var btnReset = document.getElementById('reset');
var btnRecord = document.getElementById('record');
var timeContainer = document.getElementById('lapInfo');
var countLaps = 0;
var isFirstColumn = true;
// Eventos



btnPlayStop.addEventListener('click', function () {
    paused = false;
    var sound = document.getElementById('audio');
    sound.play()

    if (play) {
        play = false;
        btnPlayStop.setAttribute('src', 'img/stop.svg');
        startChronometer();
        return;
    }
    play = true;
    paused = true;
    btnPlayStop.setAttribute('src', 'img/play.svg');
    stop();
});

btnReset.addEventListener('click', function () {
    paused = false;
    document.getElementById('audio-reset').play();
    reset();
});

btnRecord.addEventListener('click', function () {
    if (paused) return;
    if (countLaps < 7) {
        console.log(countLaps);
        document.getElementById('audio-lap').play()
        getLap();
    }
});

// Funções
function startChronometer() {

    var minPlaceHolder = `0`;
    var segPlaceHolder = `0`;
    var miliPlaceHolder = `0`;


    timer = setInterval(function () {
        mili++;
        if (mili == 99) {
            mili = 0;
            seg++;
            if (seg == 60) {
                seg = 0;
                min++;
                if (min == 60) {
                    min = 0;
                    hora++;
                }
            }
        }
        if (mili > 9) miliPlaceHolder = '';
        else miliPlaceHolder = '0';

        if (seg > 9) segPlaceHolder = '';
        else segPlaceHolder = '0';

        if (min > 9) minPlaceHolder = '';
        else minPlaceHolder = '0';

        document.getElementById('timer').innerHTML = `<span id="min">
                                                            ${minPlaceHolder}${min}:
                                                        </span>
                                                        <span id="seg">
                                                            ${segPlaceHolder}${seg}
                                                        </span>
                                                        <br>
                                                        <span id="mili">
                                                            ${miliPlaceHolder}${mili}
                                                        </span>`;
        lap = `${minPlaceHolder}${min}:${segPlaceHolder}${seg}  ${miliPlaceHolder}${mili}`;
    }, 10);
}

function stop() {
    clearInterval(timer);
}

function reset() {
    countLaps = 0;
    clearInterval(timer);
    lap = '';
    paused = true;
    timeContainer.innerHTML = '';
    if (!play) {
        btnPlayStop.setAttribute('src', 'img/play.svg');
        play = true;
    }
    mili = 0;
    seg = 0;
    min = 0;
    document.getElementById('timer').innerHTML = `<span id="min">
                                                    00:
                                                    </span>
                                                    <span id="seg">
                                                        00
                                                    </span>
                                                    <br>
                                                    <span id="mili">
                                                        00
                                                    </span>`;
}

function getLap() {
    if (countLaps > 6) {
        btnRecord.setAttribute('disabled', 'true');
        return;
    };
    countLaps++;
    timeContainer.innerHTML += `<span class="lapTimer">${countLaps} - ${lap}</span><br>`;
}

function setup() {
    document.addEventListener('keypress', (event) => {
        if (event.key.toLowerCase() == "s") btnPlayStop.click();
        if (event.key.toLowerCase() == "t") btnRecord.click();
        if (event.key.toLowerCase() == "r") btnReset.click();
    });
}
setup();
